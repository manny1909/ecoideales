const { static } = require('express');
const express=require('express');
const app=express();
const morgan=require('morgan');
const path=require('path')
//ajustes
app.set('port',process.env.PORT || 3000)
//middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(morgan("dev"));
//rutas
app.use(require("./routes/index"))

//archivos estaticos
app.use(express.static(path.join(__dirname,"public")))


module.exports = app;
